#include "hoffman.h"
#include "node.h"

std::vector<bool> hoffman(std::vector<bool> &input, std::vector<bool> &output, int bufferSize)
{
    // words frequency.
    // word itself is of vector<bool> type and the frequency is of long long type.
    std::map<std::vector<bool>, long long> wordFrequency;

    // like each word
    std::vector<bool> buffer(bufferSize);

    // calculate word frequency
    for(long long i = 0; i < input.size(); i += bufferSize)
    {
        // copy word into buffer
        for(long long j = 0; j < bufferSize; j++)
            buffer[j] = input[i + j];

        // check if word exists in map. if not so, assign it.
        if(wordFrequency.find(buffer) == wordFrequency.end())
            wordFrequency[buffer] = 0;
        
        // increase word frequency by 1
        wordFrequency[buffer] += 1;
    }

    // now we are to sort the words by frequency.
    // a map doesn't sort elements by values. so we create a vector and reload map data into it.
    // vector to store word frequency into.
    // it consists of pairs in which first element is word and second is frequency.
    std::vector<std::pair<std::vector<bool>, long long> > wordFrequency_vector;

    // an iterator to iterate over the word frequency map
    std::map<std::vector<bool>, long long> :: iterator it;

    // load word frequency map to word frequency vector.
    for(it = wordFrequency.begin(); it != wordFrequency.end(); it++)
        wordFrequency_vector.push_back(make_pair(it->first, it->second));
    
    // sort word frequency vector by frequency.
    sort(wordFrequency_vector.begin(), wordFrequency_vector.end(), sortByVal);


    // now we are to generate the hoffman tree
    auto heap = create_heap(wordFrequency_vector);

    for(auto it = heap.begin(); it < heap.end(); it++)
        std::cout<<it->left<<std::endl;

}


// a compare function to use with sort function to sort pairs by second element.
// these pairs are elements of word frequency vector.
bool sortByVal(std::pair<std::vector<bool>, long long > &a, std::pair<std::vector<bool>, long long > &b)
{
    return (a.second < b.second);
}

std::set<node> create_heap(std::vector<std::pair<std::vector<bool>, long long> > array)
{
    // create a set to add to heap.
    std::set<std::pair<std::vector<bool>, long long> > set_array;

    // first fill the set with words frequency
    for(long long i = 0; i < array.size; i++)
        set_array.insert(array[i]);

    // the heap will be stored here
    std::vector<node> heap;

    node n3, n2, n1;

    std::vector<node> nodes_vector;

    std::set<node> nodes_set;
    node n;
    for(long long i = 0; i < array.size; i++)
    {
        n.word = array[i].first;
        n.weight = array[i].second;
        n.type = 0;
        nodes_set.insert(n);
    }
    
    std::set<node> array_node_set = nodes_set;

    // define an itterator
    std::set<node>::iterator it;
    while(nodes_set.size() > 0)
    {
        it = nodes_set.begin();
        n1 = *array_node_set.find(*it);
        nodes_set.erase(it);

        it = nodes_set.begin();
        n2 = *it;

        n3.left = &n1;
        n3.right = &n2;
        n3.type = 1;
        n3.weight = n3.left->weight + n3.right->weight;
        nodes_set.insert(n3);
    }


}
