/*
    hoffman compression algorithm implementation
    code by: Erfan Kheyrollahi Qaroğlu
    https://gitlab.com/erfanekm/
    2020/01/01
*/

#include <iostream>
#include <fstream>
#include <string>
#include <vector>

#include "hoffman.h"

using namespace std;

int main(int argc, char const *argv[])
{

    // file name to read
    string filename;

    // get file name
    // get it from shell if possible
    if(argc > 1)
        filename = argv[1];
    else
        filename = "test.txt";
    
    // open the file
    ifstream inputFile (filename, ios::in | ios::binary);

    // array to load the file into. bit by bit.
    vector<bool> data;

    // to read file byte by byte
    char buffer;

    // itterator!
    int i;

    // load the file
    // read byte by byte
    while(inputFile.get(buffer))
    //convert bytes to bits and store in the array.
        for(i = 0; i < 8; i++)
            data.push_back((buffer >> i) & 1);
    
    vector<bool> compressedData;

    int bufferSize = 4;

    hoffman(data, compressedData, bufferSize);


    return 0;
}
