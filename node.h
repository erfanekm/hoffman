#include <vector>

struct node
{
    bool type = 0; // type 0 : leaf. type 1 : non leaf tree node.
    node* left;
    node* right;
    long long weight = 0;
    std::vector<bool> word;
};
