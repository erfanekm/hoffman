#pragma once

#include <vector>
#include <map>
#include <iostream>
#include <algorithm>
#include <utility>
#include <set>

// compress array by hoffman algorithm.
std::vector<bool> hoffman(std::vector<bool> &input, std::vector<bool> &output, int bufferSize);

//creates hoffman heap. works as a helper for main hoffman function
std::set<node> create_heap(std::vector<std::pair<std::vector<bool>, long long> > array);
